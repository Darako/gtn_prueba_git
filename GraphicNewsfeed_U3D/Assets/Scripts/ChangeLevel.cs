﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeLevel : MonoBehaviour {

    string path = "Config.json";

    private void Start() {
#if UNITY_EDITOR
        path = "Assets/Resources/JSon/Config.json";
#endif
    }

    public void Back() {
        if (File.Exists(path)) {
            File.Delete(path);
        }
        SceneManager.LoadScene(0);
    }
}
