﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;

public class Configurator : MonoBehaviour {

    public GameObject canvas;

    GameObject scenario;
    public GameObject cameras;
    public GameObject cameraRoutes;
    public GameObject figureHolder;
    ConfigurationData configurationData;
    string path = "Config.json";
    string json = "";

    public TimelineAsset timelineAsset;

    private void Start() {
#if UNITY_EDITOR
        path = "Assets/Resources/JSon/Config.json";
#endif
        scenario = gameObject;
        ReceiveJson(json);
        ConfigurateScene();
    }

    public void Update() {
        if (GetComponent<PlayableDirector>().state == PlayState.Paused) {
            canvas.SetActive(true);
        }
    }

    public void ReceiveJson(string json) {
        json = File.ReadAllText(path).ToString();
        configurationData = JsonUtility.FromJson<ConfigurationData>(json);
    }

    public void ConfigurateScene() {
        ConfigurateFigure(configurationData.figureNumber);
        ConfigurateColor(configurationData.colorNumber);
        ConfigurateCamera(configurationData.cameraNumber);
    }

    public void ConfigurateCamera(int cameraIndex) {
        //activating right camera
        SwitchMainCamera(cameraIndex);
        //activating right route
        SwitchCameraRoutes(cameraIndex);  
        //unmuting the right track
        SwitchCameraTracks(cameraIndex);
    }

    public void ConfigurateFigure(int figureIndex) {
        GameObject figure = new GameObject();
        switch (figureIndex) {
            case 0: { figure = GameObject.CreatePrimitive(PrimitiveType.Cube); break; }
            case 1: { figure = GameObject.CreatePrimitive(PrimitiveType.Sphere); break; }
        }
        figure.transform.SetParent(figureHolder.transform);
    }

    public void ConfigurateColor(int colorIndex) {
        GameObject figure = figureHolder.transform.GetChild(0).gameObject;
        switch (colorIndex) {
            case 0: { figure.GetComponent<MeshRenderer>().material.color = Color.red; break; }
            case 1: { figure.GetComponent<MeshRenderer>().material.color = Color.green; break; }
            case 2: { figure.GetComponent<MeshRenderer>().material.color = Color.blue; break; }
        }       
    }

    private void SwitchCameraTracks(int cameraIndex) {
        PlayableDirector pd = scenario.GetComponent<PlayableDirector>();
        pd.playableAsset = Resources.Load("Timeline/Route" + (cameraIndex + 1)) as PlayableAsset;
        pd.Play();
    }

    private void SwitchMainCamera(int cameraIndex) {
        for (int i = 0; i < cameras.transform.childCount; i++) {
            cameras.transform.GetChild(i).gameObject.SetActive(false);
        }
        cameras.transform.GetChild(cameraIndex).gameObject.SetActive(true);
    }

    private void SwitchCameraRoutes(int cameraIndex) {
        for (int i = 0; i < cameraRoutes.transform.childCount; i++) {
            cameraRoutes.transform.GetChild(i).gameObject.SetActive(false);
        }
        cameraRoutes.transform.GetChild(cameraIndex).gameObject.SetActive(true);
    }

    
}
