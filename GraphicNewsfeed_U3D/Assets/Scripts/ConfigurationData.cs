﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigurationData {

    [SerializeField] public int cameraNumber;
    [SerializeField] public int figureNumber;
    [SerializeField] public int colorNumber;

    public ConfigurationData(int cameraNumber, int figureNumber, int colorNumber) {
        this.cameraNumber = cameraNumber;
        this.figureNumber = figureNumber;
        this.colorNumber = colorNumber;
    }
}
