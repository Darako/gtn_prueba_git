﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageCarousel : MonoBehaviour {

    public List<Sprite> imageList;
    public Image imageHolder;
    public Button upButton;
    public Button downButton;
    public int carouselIndex = 0;
    int maxImages;

    // Use this for initialization
    void Start () {
        carouselIndex = 0;
        maxImages = imageList.Count;
        upButton.onClick.AddListener(ImageUp);
        downButton.onClick.AddListener(ImageDown);
    }

    public void ImageUp() {
        carouselIndex = ((carouselIndex + 1) >= maxImages) ? 0 : (carouselIndex+1);
        ChangeImage(carouselIndex);
    }

    public void ImageDown() {
        carouselIndex = ((carouselIndex - 1) < 0) ? (maxImages - 1) : (carouselIndex-1);
        ChangeImage(carouselIndex);
    }

    private void ChangeImage(int imageIndex) {
        imageHolder.sprite = imageList[imageIndex];
    }
}