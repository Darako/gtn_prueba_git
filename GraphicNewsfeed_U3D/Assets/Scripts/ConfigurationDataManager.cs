﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConfigurationDataManager : MonoBehaviour {

    //[SerializeField] public ConfigurationElements configElem;

    public ImageCarousel imageCarousel;
    public Dropdown figureDropdown;
    public Dropdown colorDropdown;
    public Button loadButton;
    string path = "Config.json";
    int cameraNumber;
    int figureNumber;
    int colorNumber;

    private void Start() {
#if UNITY_EDITOR
        path = "Assets/Resources/JSon/Config.json";
#endif
    }

    public void ConfigToJson() {
        ConfigurationData configElem = new ConfigurationData(cameraNumber, figureNumber, colorNumber);
        string json = JsonUtility.ToJson(configElem);
        SaveFile(json);        
    }

    public void RenderScene() {
        //StartCoroutine(LoadAsyncScene());
        LoadScene();
    }

    private void LoadScene() {
        SceneManager.LoadScene(1);
    }
    private IEnumerator LoadAsyncScene() {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("RenderScene");
        while (!File.Exists(path) && !asyncLoad.isDone) {
            yield return null;
        }
    }

    private void SaveFile(string json) {
        StartCoroutine(SaveFileAsync(json));
    }

    private IEnumerator SaveFileAsync(string json) {
        using (FileStream fs = new FileStream(path, FileMode.Create)) {
            using (StreamWriter writer = new StreamWriter(fs)) {
                writer.Write(json);                
            }
        }
        yield return new WaitUntil(() => File.Exists(path));
        loadButton.interactable = true;
    }

    public void RetrieveData() {
        cameraNumber = RetrieveCameraNumber();
        figureNumber = RetrieveFigureNumber();
        colorNumber = RetrieveColorNumber();
        ConfigToJson();
    }

    int RetrieveCameraNumber() {
        return imageCarousel.carouselIndex;
    }
    int RetrieveFigureNumber() {
        return figureDropdown.value; ;
    }
    int RetrieveColorNumber() { 
        return colorDropdown.value;
    }
}